# Module: deploy

此模块包含各种项目部署任务。

虽然中间件部署模块中一般包含有部署相应项目的define，例如：tomcat模块中有tomcat::war的define来实现war包部署。但是，foreman中无法实例化define，只能通过class来应用各项配置。所以，此模块使用专门的class（类），来实例化中间件模块中的define，达到部署相应项目的目的。

同时，开发此模块的目的也是为了各种项目的部署整合到一个class中来。

* war包的部署
* http项目部署（todo...）
* etc.

## 依赖的模块

1. puppetlabs-vcsrepo
1. puppetlabs-concat
1. puppetlabs-stdlib
2. nanliu-staging
3. tomcat

## 使用方法

### war部署

* 自动安装yum源中可找到的最新java，所以请确保yum可用。
* 默认安装7.0.63版本的tomcat。如需安装其他版本的tomcat，或定制tomcat参数，可应用[tomcat](http://git.oschina.net/cocox/pupept-tomcat)模块，并配置项目参数。
* 可一次性部署多个war包，并配置项目参数。

```
class { 'deploy':
  tomcat_projects =>
    { 'jenkins' => { 'war_source' => 'ftp://puppet.ht.com/war/jenkins.war',
                     'contexts'   => { 'path' => '/', 'base' => 'jenkins', 'reloadable' => true },
                   },
      'foo'     => { 'version' => '1.2.3' },
    }
}
```

foreman中，hash的写法只支持ymal和json，所以，上例中`tomcat_projecs`参数值的写法为：
```
jenkins:
  war_source: ftp://puppet.ht.com/war/jenkins.war
  contexts:
    path: "/"
    base: jenkins
    reloadable: true
foo:
  version: '1.2.3'
```

参数tomcat_projects的值是一个嵌套的hash。

第一层是`项目名->项目参数`的`key->value`对,有多个项目时，可以定义多个。

第二层是所有具体项目参数的`key->value`对。对于war项目，可用的参数有`war_source`、`version`、`contexts`。

* `war_source` 字符串，默认`undef`。表示下载war包的URL，可以是ftp://、http(s)://、puppet://、local。
* `version` 字符串，默认`undef`。定义此字符串，表示war包地址为`puppet:///modules/deploy/${title}-${version}.war`，即，下载`${modules_dir}/files/${title}-${version}.war`文件；默认值时，地址为`puppet:///modules/deploy/${title}.war`。定义了`war_source`参数时，此参数不起作用。
* `contexts` hash，默认`{ base => $title, path => $title, reloadable => false,}`。此参数定义`${CATALINA_HOME}/conf/server.xml`文件`<Context>`标签的内容。