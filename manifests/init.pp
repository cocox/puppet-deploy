#$tomcat_projects = {
#  war1 => {
#    war_source => 'http://192.168.1.173/war1.war',
#    contexts   => { base => 'war1', path => '/', reloadable => true }
#  },
#  war2 => {
#    war_source => 'http://192.168.1.173/war1.war',
#    contexts   => { base => 'war1', path => '/', reloadable => true }
#  },
#}
#
#
class deploy (
  $tomcat_projects = {},
) {
  
  if $tomcat_projects and is_hash($tomcat_projects) {
    class { 'deploy::war':
      data => $tomcat_projects,
    }
  }
}