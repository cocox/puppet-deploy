class deploy::war (
  $data = {},
) {
  $_data = hiera_hash('deploy::war::data', $data)
  validate_hash($_data)
  create_resources('deploy::resources::war', $_data)
}